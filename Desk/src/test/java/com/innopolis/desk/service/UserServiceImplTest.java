package com.innopolis.desk.service;

import com.innopolis.desk.DeskApplicationTests;
import com.innopolis.desk.form.AdvertForm;
import com.innopolis.desk.form.UserForm;
import com.innopolis.desk.model.Advert;
import com.innopolis.desk.model.Bonus;
import com.innopolis.desk.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceImplTest extends DeskApplicationTests {

    @Autowired
    private UserServiceImpl service;
    @Autowired
    private AdvertService advertService;

    @Transactional
    @Test
    void testAddUser() {
        UserForm userForm = UserForm.builder()
                        .name("Lev")
                        .phoneNumber("+7174267162")
                        .build();
        User user = service.addUser(userForm);
        assertNotNull(user.getId());
        assertEquals(user.getName(), userForm.getName());
        assertEquals(user.getPhoneNumber(), userForm.getPhoneNumber());
    }

    @Test
    void testGetAll() {
        List<User> userList = service.getAll();
        assertNotNull(userList);
    }

    @Transactional
    @Test
    void deleteUser() {
        UserForm userForm = UserForm.builder()
                .name("Tim")
                .phoneNumber("+72736472")
                .build();
        User user = service.addUser(userForm);
        service.deleteUser(user.getId());
    }

    @Transactional
    @Test
    void testGetUser() {
        UserForm userForm = UserForm.builder()
                .name("Kolya")
                .phoneNumber("+792374813")
                .build();
        User user = service.addUser(userForm);
        User getUser = service.getUser(user.getId());
        assertEquals(user.getId(), getUser.getId());
    }

    @Transactional
    @Test
    void testUpdate() {
        UserForm userForm = UserForm.builder()
                .name("Petya")
                .phoneNumber("+776347122")
                .build();
        User user = service.addUser(userForm);

        UserForm userFormToUpdate = UserForm.builder()
                .name("Lina")
                .phoneNumber("+81864192")
                .build();

        service.update(user.getId(), userFormToUpdate);
        assertNotNull(user);
        assertEquals(userFormToUpdate.getName(), user.getName());
        assertEquals(userFormToUpdate.getPhoneNumber(), user.getPhoneNumber());
    }

    @Transactional
    @Test
    void getAdvertsOfUser() {
        UserForm userForm = UserForm.builder()
                .name("Yana")
                .phoneNumber("+78123617")
                .build();
        User user = service.addUser(userForm);

        AdvertForm advertForm = AdvertForm.builder()
                .category("Car")
                .price(1000)
                .title("Test Car")
                .description("Car for sell")
                .build();

        advertService.addAdvert(advertForm, user.getId());

        List<Advert> advertsOfUser = service.getAdvertsOfUser(user.getId());
        assertEquals(advertsOfUser.stream().findFirst().get().getTitle(), advertForm.getTitle());
        assertEquals(advertsOfUser.stream().findFirst().get().getPrice(), advertForm.getPrice());
        assertEquals(advertsOfUser.stream().findFirst().get().getDescription(), advertForm.getDescription());
    }
}