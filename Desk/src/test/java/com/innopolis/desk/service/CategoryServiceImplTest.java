package com.innopolis.desk.service;

import com.innopolis.desk.DeskApplicationTests;
import com.innopolis.desk.form.AdvertForm;
import com.innopolis.desk.form.CategoryForm;
import com.innopolis.desk.model.Advert;
import com.innopolis.desk.model.Category;
import com.innopolis.desk.model.User;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CategoryServiceImplTest extends DeskApplicationTests {

    @Autowired
    private CategoryServiceImpl service;
    @Autowired
    private AdvertServiceImpl advertService;
    @Autowired
    private UserServiceImpl userService;

    @Test
    void testGetAll() {
        List<Category> categories = service.getAll();
        assertNotNull(categories);
    }

    @Transactional
    @Test
    void testGetAdvertsById() {
        List<Category> categories = service.getAll();
        Category category = categories.stream().findFirst().get();
        User user = userService.getAll().stream().findFirst().get();

        AdvertForm advertForm = AdvertForm.builder()
                .category(category.getName())
                .price(90000)
                .title("Test car")
                .description("New car")
                .build();

        Advert advert = advertService.addAdvert(advertForm, user.getId());

        List<Advert> advertsById = service.getAdvertsById(category.getId());

        assertNotNull(advertsById);
        assertTrue(advertsById.contains(advert));
    }

    @Transactional
    @Test
    void testGetCategory() {
        List<Category> categories = service.getAll();
        Category category = categories.stream().findFirst().get();
        Category categoryById = service.getCategory(category.getId());

        assertNotNull(categoryById);
        assertEquals(category.getName(), categoryById.getName());
        assertEquals(category.getId(), categoryById.getId());
    }

    @Test
    void testAddCategory() {
        CategoryForm testForm = CategoryForm.builder().name("Test").build();
        Category category = service.addCategory(testForm);
        assertEquals(testForm.getName(), category.getName());
    }

    @Test
    void testDeleteCategory() {
        CategoryForm testForm = CategoryForm.builder().name("Test").build();
        Category category = service.addCategory(testForm);
        service.deleteCategory(category.getId());
    }
}