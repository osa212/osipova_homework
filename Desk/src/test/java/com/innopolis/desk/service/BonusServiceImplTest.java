package com.innopolis.desk.service;

import com.innopolis.desk.DeskApplicationTests;
import com.innopolis.desk.model.Bonus;
import com.innopolis.desk.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;

class BonusServiceImplTest extends DeskApplicationTests {

    @Autowired
    private BonusService service;

    @Autowired
    private UserService userService;

    @Transactional
    @Test
    void testCreateBonus() {
        User user = userService.getAll().stream().findFirst().get();
        service.createBonus(user.getId());

        assertNotNull(user.getBonus());
    }

    @Transactional
    @Test
    void testGetNewBonus() {
        User user = userService.getAll().stream().findFirst().get();
        Bonus bonus = user.getBonus();

        Integer initialSum = bonus.getSum();
        service.getNewBonus(user.getId(), "0");

        assertNotEquals(bonus.getSum(), initialSum);
    }

    @Transactional
    @Test
    void testGenerateBonusNumbers() {
        User user = userService.getAll().stream().findFirst().get();
        Bonus bonus = user.getBonus();

        Integer number = bonus.getNumberOne();
        service.generateBonusNumbers(user.getId());

        assertNotEquals(bonus.getNumberOne(), number);
    }
}