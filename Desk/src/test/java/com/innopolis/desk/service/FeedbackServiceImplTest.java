package com.innopolis.desk.service;

import com.innopolis.desk.DeskApplicationTests;
import com.innopolis.desk.model.Feedback;
import com.innopolis.desk.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FeedbackServiceImplTest extends DeskApplicationTests {

    @Autowired
    private FeedbackService service;

    @Autowired
    private UserService userService;

    @Test
    void testCreateFeedback() {
        User user = userService.getAll().stream().findFirst().get();
        Feedback message = service.createFeedback("Test message", user.getId());

        assertNotNull(message);
        assertEquals("Test message", message.getMessage());
    }

    @Transactional
    @Test
    void getFeedbackById() {
        User user = userService.getAll().stream().findFirst().get();
        Feedback message = service.createFeedback("Test message", user.getId());

        List<Feedback> feedbackById = service.getFeedbackById(user.getId());
        assertEquals(user.getId(), feedbackById.stream().findFirst().get().getUser().getId());
    }

    @Test
    void delete() {
        User user = userService.getAll().stream().findFirst().get();
        Feedback message = service.createFeedback("Test message", user.getId());
        service.delete(message.getId());
    }
}