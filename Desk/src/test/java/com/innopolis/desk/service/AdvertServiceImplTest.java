package com.innopolis.desk.service;

import com.innopolis.desk.DeskApplicationTests;
import com.innopolis.desk.form.AdvertForm;
import com.innopolis.desk.form.UserForm;
import com.innopolis.desk.model.Advert;
import com.innopolis.desk.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AdvertServiceImplTest extends DeskApplicationTests {

    @Autowired
    private AdvertService service;

    @Autowired
    private UserService userService;

    private User testUser;

    @BeforeEach
    public void init() {
        UserForm userForm = UserForm.builder()
                .name("Dima")
                .phoneNumber("+7777777777")
                .build();
        testUser = userService.addUser(userForm);
    }

    @Transactional
    @Test
    void testGetAll() {
        List<Advert> advertList = service.getAll();
        assertNotNull(advertList);
    }

    @Transactional
    @Test
    void testGetAdvert() {
        Advert createdAdvert = createAdvert();

        Advert getAdvert = service.getAdvert(createdAdvert.getId());
        assertEquals(getAdvert.getId(), createdAdvert.getId());
    }

    @Transactional
    @Test
    void testAddAdvert() {
        AdvertForm advertForm = AdvertForm.builder()
                .category("Electronic")
                .price(1000)
                .title("Test phone")
                .description("New phone")
                .build();

        Advert createdAdvert = service.addAdvert(advertForm, testUser.getId());

        assertNotNull(createdAdvert);
        assertNotNull(createdAdvert.getUser());
        assertNotNull(createdAdvert.getId());

        assertEquals(createdAdvert.getTitle(), advertForm.getTitle());
        assertEquals(createdAdvert.getDescription(), advertForm.getDescription());
        assertEquals(createdAdvert.getPrice(), advertForm.getPrice());
    }

    @Transactional
    @Test
    void deleteAdvert() {
        Advert advert = createAdvert();
        service.deleteAdvert(advert.getId());
    }

    @Transactional
    @Test
    void update() {
        Advert advert = createAdvert();

        AdvertForm advertForm = AdvertForm.builder()
                .category("Electronic")
                .price(1200)
                .title("Test phone")
                .description("New phone")
                .build();

        service.update(advert.getId(), advertForm);
        assertEquals(advert.getPrice(), advertForm.getPrice());
    }

    private Advert createAdvert() {
        AdvertForm advertForm = AdvertForm.builder()
                .category("Electronic")
                .price(1000)
                .title("Test phone")
                .description("New phone")
                .build();

        Advert createdAdvert = service.addAdvert(advertForm, testUser.getId());

        return createdAdvert;
    }
}