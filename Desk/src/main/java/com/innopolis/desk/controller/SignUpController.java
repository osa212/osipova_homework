package com.innopolis.desk.controller;

import com.innopolis.desk.form.SignUpForm;
import com.innopolis.desk.service.SignUpService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequiredArgsConstructor
@Controller
@RequestMapping("/signUp")
public class SignUpController {

    private final SignUpService signUpService;

    @GetMapping
    public String getSignUpPage() {
        return "sign/user_signup";
    }

    @PostMapping
    public String signUpUser(SignUpForm form) {
        signUpService.signUp(form);
        return "redirect:/signIn";
    }
}
