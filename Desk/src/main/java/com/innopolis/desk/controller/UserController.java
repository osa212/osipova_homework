package com.innopolis.desk.controller;

import com.innopolis.desk.form.UserForm;
import com.innopolis.desk.model.Advert;
import com.innopolis.desk.model.User;
import com.innopolis.desk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public String getUsers(Model model) {
        List<User> users = userService.getAll();
        model.addAttribute(users);
        return "users/users";
    }

    @GetMapping("/users/{user-id}")
    public String getUserPage(Model model, @PathVariable("user-id") Integer userId) {
        User user = userService.getUser(userId);
        model.addAttribute("user", user);
        return "users/user";
    }

    @GetMapping("/users/{user-id}/adverts")
    public String getAdvertsOfUser(Model model, @PathVariable("user-id") Integer userId) {
        List<Advert> adverts = userService.getAdvertsOfUser(userId);
        model.addAttribute("adverts", adverts);
        model.addAttribute("userId", userId);
        return "users/adverts_of_user";
    }

    @GetMapping("/users/new")
    public String addUserPage() {
        return "users/add_user";
    }

    @PostMapping("/users/new")
    public String addUser(@Valid UserForm userForm) {
        userService.addUser(userForm);
        return "redirect:/users";
    }

    @PostMapping("/users/{user-id}/delete")
    public String deleteUser(@PathVariable("user-id") Integer userId) {
        userService.deleteUser(userId);
        return "redirect:/users";
    }

    @GetMapping("/users/{user-id}/update")
    public String getUpdatePage(Model model, @PathVariable("user-id") Integer userId) {
        User user = userService.getUser(userId);
        model.addAttribute(user);
        return "users/user";
    }

    @PostMapping("/users/{user-id}/update")
    public String update(@PathVariable("user-id") Integer userId, @Valid UserForm userForm) {
        userService.update(userId, userForm);
        return "redirect:/users/" + userId;
    }

}
