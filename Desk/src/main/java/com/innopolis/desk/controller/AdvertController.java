package com.innopolis.desk.controller;

import com.innopolis.desk.form.AdvertForm;
import com.innopolis.desk.model.Advert;
import com.innopolis.desk.service.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class AdvertController {
    private final AdvertService advertService;

    @Autowired
    public AdvertController(AdvertService advertService) {
        this.advertService = advertService;
    }

    @GetMapping("/adverts")
    public String getAdverts(Model model) {
        List<Advert> adverts = advertService.getAll();
        model.addAttribute(adverts);
        return "adverts/adverts";
    }

    @GetMapping("/adverts/{advert-id}")
    public String getAdvert(Model model, @PathVariable("advert-id") Integer advertId) {
        Advert advert = advertService.getAdvert(advertId);
        model.addAttribute("advert", advert);
        return "adverts/advert";
    }

    @GetMapping("/users/{user-id}/adverts/new")
    public String newAdvertPage(Model model, @PathVariable("user-id") Integer userId) {
        model.addAttribute("userId", userId);
        return "adverts/add_advert";
    }

    @PostMapping("/users/{user-id}/adverts/new")
    public String addAdvert(Model model, @Valid AdvertForm advertForm, @PathVariable("user-id") Integer userId) {
        advertService.addAdvert(advertForm, userId);
        model.addAttribute("userId", userId);

        return "redirect:/adverts";
    }

    @PostMapping("/adverts/{advert-id}")
    public String deleteAdvert(@PathVariable("advert-id") Integer advertId) {
        advertService.deleteAdvert(advertId);
        return "redirect:/adverts";
    }

    @GetMapping("/adverts/{advert-id}/update")
    public String updatePage(Model model, @PathVariable("advert-id") Integer advertId) {
        model.addAttribute("advertId", advertId);
        return "adverts/update_advert";
    }

    @PostMapping("/adverts/{advert-id}/update")
    public String update(Model model, AdvertForm advertForm, @PathVariable("advert-id") Integer advertId) {
        advertService.update(advertId, advertForm);
        model.addAttribute("advertId", advertId);
        return "redirect:/adverts";
    }
}
