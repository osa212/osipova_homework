package com.innopolis.desk.controller;

import com.innopolis.desk.model.Bonus;
import com.innopolis.desk.model.User;
import com.innopolis.desk.service.BonusService;
import com.innopolis.desk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class BonusController {

    private final BonusService bonusService;
    private final UserService userService;

    @Autowired
    public BonusController(BonusService bonusService, UserService userService) {
        this.bonusService = bonusService;
        this.userService = userService;
    }

    @GetMapping("/users/{user-id}/bonus")
    public String getBonusPage(Model model, @PathVariable("user-id") Integer userId) {

        Bonus bonus = bonusService.generateBonusNumbers(userId);

        model.addAttribute("bonus", bonus);
        model.addAttribute("user", userId);

        model.addAttribute("numberOne", bonus.getNumberOne());
        model.addAttribute("numberTwo", bonus.getNumberTwo());

        return "bonuses/bonus";
    }

    @PostMapping("/users/{user-id}/bonus")
    public String getNewBonus(Model model, String answer, @PathVariable("user-id") Integer userId) {
        bonusService.getNewBonus(userId, answer);

        User user = userService.getUser(userId);
        model.addAttribute("user", user);

        return "redirect:/users/" + userId + "/bonus";
    }
}
