package com.innopolis.desk.controller;

import com.innopolis.desk.model.Feedback;
import com.innopolis.desk.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class FeedbackController {

    private final FeedbackService feedbackService;

    @Autowired
    public FeedbackController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @GetMapping("/users/{user-id}/feedback")
    public String getFeedbackPage(Model model, @PathVariable("user-id") Integer userId) {
        model.addAttribute("user", userId);

        List<Feedback> feedbacksById = feedbackService.getFeedbackById(userId);
        model.addAttribute(feedbacksById);

        return "feedback/feedback";
    }

    @PostMapping("/users/{user-id}/feedback")
    public String sendFeedback(Model model, String message, @PathVariable("user-id") Integer userId) {

        feedbackService.createFeedback(message, userId);

        model.addAttribute("user", userId);

        return "redirect:/users/" + userId;
    }

    @PostMapping("/users/{user-id}/feedback/{feedback-id}")
    public String deleteFeedback(@PathVariable("feedback-id") Integer feedbackId,
                                 @PathVariable("user-id") Integer userId,
                                 Model model) {
        feedbackService.delete(feedbackId);
        model.addAttribute("user", userId);
        return "redirect:/users/" + userId +"/feedback";
    }

}
