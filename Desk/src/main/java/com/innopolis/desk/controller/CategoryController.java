package com.innopolis.desk.controller;

import com.innopolis.desk.form.CategoryForm;
import com.innopolis.desk.model.Advert;
import com.innopolis.desk.model.Category;
import com.innopolis.desk.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CategoryController {
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/categories")
    public String getCategories(Model model) {
        List<Category> categories = categoryService.getAll();
        model.addAttribute(categories);
        return "categories/categories";
    }

    @GetMapping("/categories/{category-id}")
    public String getAdvertsByCategories(Model model, @PathVariable("category-id") Integer categoryId) {
        List<Advert> adverts = categoryService.getAdvertsById(categoryId);
        Category category = categoryService.getCategory(categoryId);
        model.addAttribute("adverts", adverts);
        model.addAttribute("category", category.getName());
        return "categories/adverts_by_category";
    }

    @PostMapping("/categories")
    public String addCategory(@Valid CategoryForm categoryForm) {
        categoryService.addCategory(categoryForm);
        return "redirect:/categories";
    }

}
