package com.innopolis.desk.repository;

import com.innopolis.desk.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Category getCategoryByName(String name);
}
