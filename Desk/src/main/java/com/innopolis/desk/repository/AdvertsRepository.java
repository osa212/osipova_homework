package com.innopolis.desk.repository;

import com.innopolis.desk.model.Advert;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdvertsRepository extends JpaRepository<Advert, Integer> {
    List<Advert> findAllByUserId(Integer id);
    List<Advert> findAllByCategoryId(Integer id);
}
