package com.innopolis.desk.repository;

import com.innopolis.desk.model.Bonus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BonusRepository extends JpaRepository<Bonus, Integer> {
}
