package com.innopolis.desk.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table
public class Bonus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer sum;

    private Integer numberOne;
    private Integer numberTwo;

    public Bonus(Integer sum, Integer numberOne, Integer numberTwo) {
        this.sum = sum;
        this.numberOne = numberOne;
        this.numberTwo = numberTwo;
    }
}
