package com.innopolis.desk.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "adverts")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Advert {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne()
    @JoinColumn(name = "userid")
    private User user;

    private String title;

    @ManyToOne()
    @JoinColumn(name = "categoryid")
    private Category category;

    private Integer price;
    private String description;
    private LocalDateTime date;
}
