package com.innopolis.desk.form;

import lombok.Data;

@Data
public class SignUpForm {
    private String name;
    private String phoneNumber;
    private String email;
    private String password;
}
