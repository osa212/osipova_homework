package com.innopolis.desk.form;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Builder
@Data
public class UserForm {
    @NotEmpty
    private String name;
    @NotEmpty
    private String phoneNumber;
}
