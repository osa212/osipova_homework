package com.innopolis.desk.form;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CategoryForm {
    private String name;
}
