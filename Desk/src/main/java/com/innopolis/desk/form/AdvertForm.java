package com.innopolis.desk.form;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Builder
@Data
public class AdvertForm {
    @NotEmpty
    private String title;
    @NotEmpty
    private String category;
    private Integer price;
    @NotEmpty
    private String description;
}
