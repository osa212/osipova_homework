package com.innopolis.desk.service;

import com.innopolis.desk.form.UserForm;
import com.innopolis.desk.model.Advert;
import com.innopolis.desk.model.Feedback;
import com.innopolis.desk.model.User;
import com.innopolis.desk.repository.AdvertsRepository;
import com.innopolis.desk.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserServiceImpl implements UserService {

    private final UsersRepository usersRepository;
    private final AdvertsRepository advertsRepository;
    private final BonusService bonusService;
    private final FeedbackService feedbackService;

    @Autowired
    public UserServiceImpl(UsersRepository usersRepository, AdvertsRepository advertsRepository, BonusService bonusService, FeedbackService feedbackService) {
        this.usersRepository = usersRepository;
        this.advertsRepository = advertsRepository;
        this.bonusService = bonusService;
        this.feedbackService = feedbackService;
    }

    @Override
    public User addUser(UserForm userForm) {

        User user = User.builder()
                .name(userForm.getName())
                .phoneNumber(userForm.getPhoneNumber())
                .build();

        usersRepository.save(user);
        bonusService.createBonus(user.getId());

        return user;
    }

    @Override
    public List<User> getAll() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUser(Integer userId) {
        List<Feedback> feedbackById = feedbackService.getFeedbackById(userId);
        for (Feedback feedback: feedbackById) {
            feedbackService.delete(feedback.getId());
        }
        List<Advert> allAdvertsByUserId = advertsRepository.findAllByUserId(userId);
        for (Advert advert: allAdvertsByUserId) {
            advertsRepository.delete(advert);
        }
        usersRepository.deleteById(userId);
    }

    @Override
    public User getUser(Integer userId) {

        return usersRepository.getById(userId);
    }

    @Override
    public User update(Integer userId, UserForm userForm) {
        User user = getUser(userId);
        user.setName(userForm.getName());
        user.setPhoneNumber(userForm.getPhoneNumber());
        return usersRepository.save(user);
    }

    @Override
    public List<Advert> getAdvertsOfUser(Integer userId) {
        return advertsRepository.findAllByUserId(userId);
    }
}
