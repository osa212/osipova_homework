package com.innopolis.desk.service;

import com.innopolis.desk.form.SignUpForm;
import com.innopolis.desk.model.User;
import com.innopolis.desk.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final UsersRepository repository;
    private final BonusService bonusService;

    @Override
    public void signUp(SignUpForm form) {
        User user = User.builder()
                .name(form.getName())
                .phoneNumber(form.getPhoneNumber())
                .email(form.getEmail())
                .role(User.Role.USER)
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .build();
        repository.save(user);
        bonusService.createBonus(user.getId());

    }
}
