package com.innopolis.desk.service;

import com.innopolis.desk.model.Feedback;
import com.innopolis.desk.repository.FeedbackRepository;
import com.innopolis.desk.repository.UsersRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository repository;
    private final UsersRepository usersRepository;

    public FeedbackServiceImpl(FeedbackRepository repository, UsersRepository usersRepository) {
        this.repository = repository;
        this.usersRepository = usersRepository;
    }

    @Override
    public Feedback createFeedback(String message, Integer userId) {
        Feedback feedback = Feedback.builder()
                .message(message)
                .user(usersRepository.getById(userId))
                .date(LocalDateTime.now())
                .build();
        repository.save(feedback);
        return feedback;
    }

    @Override
    public List<Feedback> getFeedbackById(Integer userId) {
        return repository.getFeedbacksByUserId(userId);
    }

    @Override
    public void delete(Integer feedbackId) {
        repository.deleteById(feedbackId);
    }

}
