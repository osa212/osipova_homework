package com.innopolis.desk.service;

import com.innopolis.desk.model.Bonus;


public interface BonusService {
    Bonus createBonus(Integer id);
    void getNewBonus(Integer userId, String answer);
    Bonus generateBonusNumbers(Integer userId);
}
