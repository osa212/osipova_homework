package com.innopolis.desk.service;

import com.innopolis.desk.form.SignUpForm;

public interface SignUpService {
    void signUp(SignUpForm form);
}
