package com.innopolis.desk.service;

import com.innopolis.desk.form.CategoryForm;
import com.innopolis.desk.model.Advert;
import com.innopolis.desk.model.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();
    List<Advert> getAdvertsById(Integer categoryId);
    Category getCategory(Integer categoryId);
    Category addCategory(CategoryForm categoryForm);
    void deleteCategory(Integer categoryId);
}
