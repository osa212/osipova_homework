package com.innopolis.desk.service;

import com.innopolis.desk.form.AdvertForm;
import com.innopolis.desk.model.Advert;
import com.innopolis.desk.model.Category;
import com.innopolis.desk.model.User;
import com.innopolis.desk.repository.AdvertsRepository;
import com.innopolis.desk.repository.CategoryRepository;
import com.innopolis.desk.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class AdvertServiceImpl implements AdvertService {

    private final AdvertsRepository advertsRepository;
    private final UsersRepository usersRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public AdvertServiceImpl(AdvertsRepository advertsRepository, UsersRepository usersRepository, CategoryRepository categoryRepository) {
        this.advertsRepository = advertsRepository;
        this.usersRepository = usersRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Advert> getAll() {
        return advertsRepository.findAll();
    }

    @Override
    public Advert getAdvert(Integer advertId) {
        return advertsRepository.getById(advertId);
    }

    @Override
    public Advert addAdvert(AdvertForm advertForm, Integer userId) {
        User user = usersRepository.getById(userId);

        Category defaultCategory = categoryRepository.getById(4);

        String categoryName = advertForm.getCategory();
        if (categoryRepository.getCategoryByName(categoryName) instanceof Category) {
            defaultCategory = categoryRepository.getCategoryByName(categoryName);
        }

        Advert advert = Advert.builder()
                        .title(advertForm.getTitle())
                        .category(defaultCategory)
                        .description(advertForm.getDescription())
                        .price(advertForm.getPrice())
                        .user(user)
                        .date(LocalDateTime.now())
                        .build();

        advertsRepository.save(advert);
        return advert;
    }

    @Override
    public void deleteAdvert(Integer advertId) {
        advertsRepository.deleteById(advertId);
    }

    @Override
    public void update(Integer advertId, AdvertForm advertForm) {
        Advert advert = advertsRepository.getById(advertId);

        advert.setTitle(advertForm.getTitle());
        advert.setDescription(advertForm.getDescription());
        advert.setPrice(advertForm.getPrice());

        advertsRepository.save(advert);
    }
}
