package com.innopolis.desk.service;

import com.innopolis.desk.form.AdvertForm;
import com.innopolis.desk.model.Advert;

import java.util.List;

public interface AdvertService {
    List<Advert> getAll();
    Advert getAdvert(Integer advertId);
    Advert addAdvert(AdvertForm advertForm, Integer userId);
    void deleteAdvert(Integer advertId);
    void update(Integer advertId, AdvertForm advertForm);
}
