package com.innopolis.desk.service;

import com.innopolis.desk.model.Feedback;

import java.util.List;

public interface FeedbackService {
    Feedback createFeedback(String message, Integer userId);
    List<Feedback> getFeedbackById(Integer userId);
    void delete(Integer feedbackId);
}
