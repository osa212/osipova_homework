package com.innopolis.desk.service;

import com.innopolis.desk.form.UserForm;
import com.innopolis.desk.model.Advert;
import com.innopolis.desk.model.User;

import java.util.List;

public interface UserService {
    User addUser(UserForm userForm);
    void deleteUser(Integer userId);
    User getUser(Integer userId);
    User update(Integer userId, UserForm userForm);
    List<User> getAll();
    List<Advert> getAdvertsOfUser(Integer userId);
}
