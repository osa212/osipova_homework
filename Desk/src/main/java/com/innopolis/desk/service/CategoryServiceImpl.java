package com.innopolis.desk.service;

import com.innopolis.desk.form.CategoryForm;
import com.innopolis.desk.model.Advert;
import com.innopolis.desk.model.Category;
import com.innopolis.desk.repository.AdvertsRepository;
import com.innopolis.desk.repository.CategoryRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final AdvertsRepository advertsRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository, AdvertsRepository advertsRepository) {
        this.categoryRepository = categoryRepository;
        this.advertsRepository = advertsRepository;
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    @Override
    public List<Advert> getAdvertsById(Integer categoryId) {
        return advertsRepository.findAllByCategoryId(categoryId);
    }

    @Override
    public Category getCategory(Integer categoryId) {
        return categoryRepository.getById(categoryId);
    }

    @Override
    public Category addCategory(CategoryForm categoryForm) {
        Category category = Category.builder()
                            .name(categoryForm.getName())
                            .build();

        categoryRepository.save(category);
        return category;
    }

    @Override
    public void deleteCategory(Integer categoryId) {
        categoryRepository.deleteById(categoryId);
    }
}
