package com.innopolis.desk.service;

import com.innopolis.desk.model.Bonus;
import com.innopolis.desk.model.User;
import com.innopolis.desk.repository.BonusRepository;
import com.innopolis.desk.repository.UsersRepository;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;

@Component
public class BonusServiceImpl implements BonusService {

    private final BonusRepository bonusRepository;
    private final UsersRepository usersRepository;

    public BonusServiceImpl(BonusRepository bonusRepository, UsersRepository usersRepository) {
        this.bonusRepository = bonusRepository;
        this.usersRepository = usersRepository;
    }

    @Override
    public Bonus createBonus(Integer id) {
        User user = usersRepository.getById(id);
        Bonus bonus = new Bonus(0, 0,0);
        user.setBonus(bonus);
        bonusRepository.save(bonus);
        return bonus;
    }

    @Override
    public void getNewBonus(Integer userId, String answer) {
        Bonus bonus = usersRepository.getById(userId).getBonus();

        Integer userAnswer = Integer.parseInt(answer);
        Integer sum = bonus.getSum();

        sum = bonus.getNumberOne() + bonus.getNumberTwo() == userAnswer ? sum + 100 : sum - 100;

        bonus.setSum(sum);
        bonusRepository.save(bonus);
    }

    @Override
    public Bonus generateBonusNumbers(Integer userId) {
        User user = usersRepository.getById(userId);

        Integer numberOne = ThreadLocalRandom.current().nextInt(100);
        Integer numberTwo = ThreadLocalRandom.current().nextInt(100);
        user.getBonus().setNumberOne(numberOne);
        user.getBonus().setNumberTwo(numberTwo);

        bonusRepository.save(user.getBonus());

        return user.getBonus();
    }


}
