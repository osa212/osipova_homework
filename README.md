# Простое CRUD приложение на Spring

На главной странице сервиса отображается список объявлений, доступны CRUD операции.

- Архитектура MVC
- База данных PostgreSQL
- Взаимодействие с базой данных при помощи Hibernate
- Для сборки проекта используется Maven
- Взаимодействие с пользователем реализовано через Freemarker
- Unit тесты для сервисного слоя
- Spring Security

[![Screenshot-2021-12-21-at-23-28-01.png](https://i.postimg.cc/YSCNSBd2/Screenshot-2021-12-21-at-23-28-01.png)](https://postimg.cc/RW2JPyr2)
